﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Элементы.ГруппаНайденыеДанные.Видимость = ЗначениеЗаполнено(ПолеШтрихкодEN13);	
	Элементы.ДругиеРазмеры.Видимость = ЗначениеЗаполнено(ПолеШтрихкодEN13);
КонецПроцедуры


#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПолеШтрихкодEN13ПриИзменении(Элемент)
	Если ПолеШтрихкодEN13Старый = ПолеШтрихкодEN13 Тогда 
		Возврат 
	КонецЕсли;
	
	Элементы.ГруппаНайденыеДанные.Видимость = ЗначениеЗаполнено(ПолеШтрихкодEN13);	
	Элементы.ДругиеРазмеры.Видимость = ЗначениеЗаполнено(ПолеШтрихкодEN13);
	
	Если ЗначениеЗаполнено(ПолеШтрихкодEN13) Тогда 
		СтруктураДанныхШтрихкода = ПолеШтрихкодEN13ПриИзмененииНаСервере(ПолеШтрихкодEN13);
		ЗаполнитьЗначенияСвойств(ЭтаФорма,СтруктураДанныхШтрихкода);
	КонецЕсли;
	
	
	ПолеШтрихкодEN13Старый = ПолеШтрихкодEN13;
КонецПроцедуры



#КонецОбласти


#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Сфотографировать(Команда)
	Если НЕ СредстваМультимедиа.ПоддерживаетсяСканированиеШтрихКодов() Тогда 
		Сообщить("Вах! Камеры нету, как ты туда 1с поставил?"); 
		Возврат
	КонецЕсли;
	
	СредстваМультимедиа.ПоказатьСканированиеШтрихКодов("Выполняеться сканирование Штрихкода",
		Новый ОписаниеОповещения("ПослеСканированияШтрихкода",ЭтаФорма),,ТипШтрихКода.Линейный);
	
КонецПроцедуры

&НаКлиенте
Процедура ДругиеРазмеры(Команда)
	СтруктураПараметров = Новый Структура("Номенклатура",Номенклатура);
	ОткрытьФорму("РегистрСведений.АктуальныеОстаткиНоменклатуры.ФормаСписка",СтруктураПараметров,ЭтаФорма);
КонецПроцедуры


#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервереБезКонтекста
Функция ПолеШтрихкодEN13ПриИзмененииНаСервере(Знач ШтрихКод)
	
	#Область Запрос
	СтруктураВозврата = Новый Структура("Номенклатура,ХарактеристикаНоменклатуры,КоличествоОстаток,ЦенаНоменклатуры",
		Справочники.Номенклатура.ПустаяСсылка(),Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка(),0,0);
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	АктуальныеОстаткиНоменклатуры.Номенклатура,
	|	АктуальныеОстаткиНоменклатуры.ХарактеристикаНоменклатуры,
	|	АктуальныеОстаткиНоменклатуры.КоличествоОстаток,
	|	АктуальныеОстаткиНоменклатуры.ЦенаНоменклатуры
	|ИЗ
	|	РегистрСведений.АктуальныеОстаткиНоменклатуры КАК АктуальныеОстаткиНоменклатуры
	|ГДЕ
	|	АктуальныеОстаткиНоменклатуры.Штрихкод = &Штрихкод";
	Запрос.УстановитьПараметр("Штрихкод",ШтрихКод);
	
	РезультатЗапроса = Запрос.Выполнить();	

	Если РезультатЗапроса.Пустой() Тогда 
		Возврат СтруктураВозврата; 	
	КонецЕсли;
	
	#КонецОбласти
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		ЗаполнитьЗначенияСвойств(СтруктураВозврата,ВыборкаДетальныеЗаписи);					
	КонецЦикла;
	
	Возврат СтруктураВозврата;
	
КонецФункции

// ПослеСканированияШтрихкода 
// ШтрихКод - результат сканирования в виде штрихкода (строка)
// Результат - булево , если все ок "истина" иначе "ложь"
// Сообщение - Сообщения для пользователя , для информации о последнем сканировании
// ДополнительныеПараметры - структура параметров
&НаКлиенте
Процедура ПослеСканированияШтрихкода(ШтрихКод,Результат,Сообщение,ДополнительныеПараметры) Экспорт
	Если Результат Тогда 
		СредстваМультимедиа.ЗакрытьСканированиеШтрихКодов();
		ПолеШтрихкодEN13 = ШтрихКод;	
		ПолеШтрихкодEN13ПриИзменении(Неопределено);
	КонецЕсли;
КонецПроцедуры


#КонецОбласти




